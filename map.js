function displayMapData(element, cb) {
  if (!cb || !element) {
    return [];
  }
  if (Array.isArray(element) === false) {
    return [];
  }
  let result = [];
  for (let i = 0; i < element.length; i++) {
    let getEachElement = cb(element[i], i, element);
    result.push(getEachElement);
  }
  return result;
}

module.exports = displayMapData;
