let filterFn = require("../filter");

let items = [1, 2, 3, 4, 5, 5];
function cb(item, index, arr) {
  return item > 2;
}

let getResult = filterFn(items, cb);
console.log(getResult);

let testForEmptyArray = [];
let result1 = filterFn(testForEmptyArray, cb);
console.log(result1);
