let getdata = require("../flatten");

const nestedArray = [[[[[0]], [1]], [[[2], [3]]], [[4], [5]]]];
let result = getdata(nestedArray);
console.log(result);

const testModules = [[[[[0]], [1]], [[[2], [3]]], [[4], ["a"]]]];
let result1 = getdata(testModules);
console.log(result1);

const testModules1 = ["b", [[1]], [{ compamy: "Mount", name: "Blue" }]];
let result2 = getdata(testModules1);
console.log(result2);

const testModules2 = "hello ashok";
let result3 = getdata(testModules2);
console.log(result3);
