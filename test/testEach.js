getEach = require("../each");

const mulItemsWithIndex = (value, index) => {
  return value * index;
};

const items = [1, 2, 3, 4, 5, 5];
let result = getEach(items, mulItemsWithIndex);
console.log(result);

let testItem = {};
let testModule = getEach(testItem, mulItemsWithIndex);
console.log(testModule);
