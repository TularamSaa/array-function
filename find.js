function DisplayFind(elements, cb) {
  if (!Array.isArray(elements) || !elements || !cb) {
    return [];
  }
  for (let i = 0; i < elements.length; i++) {
    let item = elements[i];
    if (cb(item, i, elements)) {
      return item;
    }
  }
  return undefined;
}

module.exports = DisplayFind;
