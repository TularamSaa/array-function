function displayReduce(element, cb, startingValue) {
  if (!cb || !element || Array.isArray(element) === false) {
    return [];
  }
  if (startingValue === undefined) {
    startingValue = element[0];
  } else {
    startingValue = cb(startingValue, element[0]);
  }
  for (let i = 1; i < element.length; i++) {
    startingValue = cb(startingValue, element[i]);
  }
  return startingValue;
}

module.exports = displayReduce;
