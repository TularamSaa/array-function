function flatten(arr) {
  if (Array.isArray(arr) == false) {
    return "Invalid input";
  }
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      result = result.concat(flatten(arr[i]));
      //console.log(ret);
    } else {
      result.push(arr[i]);
    }
  }
  return result;
}

module.exports = flatten;
