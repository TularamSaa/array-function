function DisplayEach(elements, cb) {
  if (!elements || !cb) {
    return;
  }
  if (Array.isArray(elements) && typeof cb === "function") {
    result = [];
    for (let i = 0; i < elements.length; i++) {
      elements[i] = cb(elements[i], i);
      result.push(elements[i]);
    }
    return result;
  } else {
    return "Invalid Input";
  }
}

module.exports = DisplayEach;
